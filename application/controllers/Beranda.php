<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Beranda extends CI_Controller {


	public function index()
	{
		check_not_login();
		$this->load->view('template/index');
	}
}
