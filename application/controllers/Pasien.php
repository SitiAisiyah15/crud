<?php

Class Pasien extends CI_Controller {

		function __construct(){
		parent::__construct();
		$this->load->database();
		$this->load->helper(array('url'));
		$this->load->model('pasien_model');
		}
 
        public function index(){ 
            $data['tb_dokter']=$this->pasien_model->getAll();
            $data['jk'] = $this->pasien_model;
            $this->load->view('tampil_pasien',$data);
        }
     
        public function ambilData(){
            $data = $this->pasien_model->tampil_tabel();
            echo json_encode($data);
        }
     
        function ambilDataByIdPasien(){
            $id_pasien = $this->input->post('id_pasien');
            $data = $this->pasien_model->getDataByIdPasien($id_pasien);
            echo json_encode($data);
        }
     
        function hapusData(){
            $id_pasien = $this->input->post('id_pasien');
            $data = $this->pasien_model->deleteData($id_pasien);
            echo json_encode($data);
        }
     
        function tambahData(){
            $id_pasien = $this->input->post('id_pasien');
    		$nama_pasien = $this->input->post('nama_pasien');
    		$alamat_pasien = $this->input->post('alamat_pasien');
    		$nomer_pasien = $this->input->post('nomer_pasien');
    		$usia_pasien = $this->input->post('usia_pasien');
    		$jk = $this->input->post('jk');
            $id_dkt = $this->input->post('id_dkt');
     
            $data = ['id_pasien' => $id_pasien, 
                    'nama_pasien' => $nama_pasien, 
                    'alamat_pasien' => $alamat_pasien, 
                    'nomer_pasien' => $nomer_pasien, 
                    'usia_pasien'=> $usia_pasien, 
                    'jk' => $jk, 
                    'id_dkt'=>$id_dkt];

            $data['jk'] = $this->pasien_model->insertData($data);
            echo json_encode($data);
        }
     
        function perbaruiData(){

            $id_pasien = $this->input->post('id_pasien');
            $nama_pasien = $this->input->post('nama_pasien');
            $alamat_pasien = $this->input->post('alamat_pasien');
            $nomer_pasien = $this->input->post('nomer_pasien');
            $usia_pasien = $this->input->post('usia_pasien');
            $jk = $this->input->post('jk');
            $id_dkt = $this->input->post('dkt');
     
            $data = [
                'id_pasien' => $id_pasien, 
                'nama_pasien' => $nama_pasien, 
                'alamat_pasien' => $alamat_pasien, 
                'nomer_pasien' => $nomer_pasien, 
                'usia_pasien'=> $usia_pasien, 
                'jk' => $jk, 
                'id_dkt'=> $id_dkt];
                
            $data = $this->pasien_model->updateData($id_pasien,$data);
            echo json_encode($data);
        }
        
}

 