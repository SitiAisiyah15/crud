<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

  public function index()
  {
    check_already_login();
    $this->load->view('tampil_login');
  }
  public function proses()
  {
    $post = $this->input->post(null, TRUE);
    if(isset($post['login'])) {
        $this->load->model('login_model');
        $query = $this->login_model->login($post);
        if($query->num_rows() > 0) {


          $row = $query->row();
          
          $params = array(
            'userid' => $row->user_id,
            'level' => $row->level

          );
          $this->session->set_userdata($params);
          echo "<script>
            alert('Selamat, Login berhasil');
            window.location='".site_url('beranda/index')."';
          </script>";
        } else {
           echo "<script>
            alert('Login gagal');
            window.location='".site_url('login/index')."';
          </script>";
        }

    }
  }

  public function logout()
  {
    $params = array('userid', 'level');
    $this->session->unset_userdata($params);
    redirect('Login/index');
  }
}
