<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Data Kesehatan | Dashboard</title>

  <!-- Custom fonts for this theme -->
  <link href="<?php echo base_url('vendor/fontawesome-free/css/all.min.css'); ?>" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css">

  <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" >
  <link href="<?php echo base_url('bootstrap/assets/css/freelancer.min.css'); ?>" rel="stylesheet" >

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg bg-secondary text-uppercase fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger">DATA KESEHATAN</a>
      <button class="navbar-toggler navbar-toggler-right text-uppercase font-weight-bold bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <?php if($this->session->userdata('level') == 1) { ?>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo site_url('Pasien/index')?>"><h3>DATA PASIEN</a></h3>
          </li>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo site_url('Riwayat/index')?>"><h3>DATA RIWAYAT PENYAKIT</a></h3>
          </li>
          <?php } ?>
          <li class="nav-item mx-0 mx-lg-1">
            <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo site_url('Dokter/index')?>"><h3>DATA DOKTER</a></h3>
          </li>
        </ul>
      </div>
    </div>
    <div class="pull-right">
      <a href="<?=site_url('login/logout')?>" class="btn btn-danger btn-xl"><b>Logout</b></a>
      
    </div>
  </nav>

  <!-- Masthead -->
  <header class="masthead bg text-black text-center">
    <div class="container d-flex align-items-center flex-column">

      <!-- Masthead Avatar Image -->
      <img class="masthead-avatar mb-5" src="<?php echo base_url('bootstrap/assets/img/avatarr.svg'); ?>" alt="">

      <!-- Masthead Heading -->
      <h1 class="masthead-heading text-uppercase mb-0">DATA KESEHATAN</h1>

      <!-- Icon Divider -->
      <div class="divider-custom divider-dark">
        <div class="divider-custom-line"></div>
        <div class="divider-custom-line"></div>
      </div>

      <!-- Masthead Subheading -->
      <p class="masthead-subheading font-weight-dark mb-5">Data Pasien - Data Dokter - Data Riwayat Penyakit</p>


    </div>
  </header>

  <!-- Bootstrap core JavaScript -->
  
  <script src="<?php echo base_url('vendor/jquery/jquery.min.js'); ?>"></script>
  <script src="<?php echo base_url('vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script>

  <!-- Plugin JavaScript -->
  <script src="<?php echo base_url('vendor/jquery-easing/jquery.easing.min.js'); ?>"></script>

  <!-- Contact Form JavaScript -->
  <script src="<?php echo base_url('js/jqBootstrapValidation.js'); ?>"></script>
  <script src="<?php echo base_url('js/contact_me.js'); ?>"></script>

  <!-- Custom scripts for this template -->
  <script src="<?php echo base_url('js/freelancer.min.js'); ?>"></script>

</body>

</html>
