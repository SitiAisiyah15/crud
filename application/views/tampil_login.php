<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Data Kesehatan | Login</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        
        <!-- Bootstrap 3.3.2 -->
        <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" >
        <!-- Font Awesome Icons -->
        <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">  
        <!-- Theme style -->
        <link href="<?php echo base_url('assets/css/AdminLTE.min.css'); ?>" rel="stylesheet">        
        <!-- iCheck -->
        <link href="<?php echo base_url('assets/js/plugins/iCheck/square/blue.css'); ?>" rel="stylesheet"> 

    </head>
    <body class="login-page">
        <div class="login-box">
            <div class="login-logo">
                <a href="<?php echo base_url('bootstrap/assets/img/avatar.svg'); ?>" ><b>Data Kesehatan</b></a>
            </div>
            <div class="login-box-body">
                <p class="login-box-msg">Login to start your session </p>
                <form action="<?=site_url('login/proses')?>" method="post">
                    <div class="form-group has-feedback">
                        <input type="text" name="username" class="form-control" placeholder="Username"/>
                        <span class="glyphicon glyphicon-user form-control-feedback"></span>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password"/>
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    </div>
                    <div class="row">
                        <div class="col-xs-8"></div>                        
                        <div class="col-xs-4">
                            <button type="submit" name="login" class="btn btn-success btn-sm">LOGIN</button>
                        </div>
                        
                    </div>
                </form>                
            </div>
        </div>

        

       
    </body>
</html>