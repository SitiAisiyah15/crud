<?php

Class Dokter_model extends CI_Model
{
     function tampil_tabel(){
        $this->db->select('tb_dokter.*, tb_jadwal.sub_jadwal as jadwal'); 
        $this->db->from('tb_dokter');
        $this->db->join('tb_jadwal', 'tb_jadwal.id_jadwal = tb_dokter.jadwal');
        $query = $this->db->get();
        return $query->result();
    }
 
    function getDataByIdDokter($id_dokter){
        $this->db->where('id_dokter',$id_dokter); 
        return $this->db->get('tb_dokter')->result(); 
    }
 
    function deleteData($id_dokter){
        $this->db->where('id_dokter',$id_dokter); 
        $this->db->delete('tb_dokter');
    }
 
    function insertData($data){
        $this->db->insert('tb_dokter',$data); 
    }
 
    function updateData($id_dokter,$data){
        $this->db->where('id_dokter',$id_dokter); 
        $this->db->update('tb_dokter',$data); 
    }

     public function getAll() 
  {
   return $this->db->get('tb_jadwal')->result();
   }
        
}