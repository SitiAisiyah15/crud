<?php

Class Pasien_model extends CI_Model
{
	function tampil_tabel(){
        $this->db->select('tb_pasien.*, tb_dokter.nama_dokter as id_dkt'); 
        $this->db->from('tb_pasien');
        $this->db->join('tb_dokter', 'tb_dokter.id_dokter = tb_pasien.id_dkt','left');
        $query = $this->db->get();
        return $query->result();
    }
 
    function getDataByIdpasien($id_pasien){
        $this->db->where('id_pasien',$id_pasien); 
        return $this->db->get('tb_pasien')->result(); 
    }
 
    function deleteData($id_pasien){
        $this->db->where('id_pasien',$id_pasien); 
        $this->db->delete('tb_pasien');
    }
 
    function insertData($data){
        $this->db->insert('tb_pasien',$data); 
    }
 
    function updateData($id_pasien,$data){
        $this->db->where('id_pasien',$id_pasien); 
        $this->db->update('tb_pasien',$data);
         
    }

     public function getAll() 
  {
   return $this->db->get('tb_dokter')->result();
   
   }
    	
}